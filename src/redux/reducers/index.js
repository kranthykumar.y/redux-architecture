import {combineReducers} from 'redux';
import {cardReducer} from './cardReducer';

const reducers = combineReducers({
    allCards: cardReducer,
});

export default reducers;