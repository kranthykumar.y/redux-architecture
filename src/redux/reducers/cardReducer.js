import { ActionTypes } from "../constants/actionTypes";

const intialState = {
    cards: []
}

export const cardReducer = (state = intialState, {type, payload}) => {
    switch (type) {
        case ActionTypes.SET_CARDS:
            return {...state, cards:payload};
        default:
            return state;
    }
}