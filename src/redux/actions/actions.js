import { ActionTypes } from "../constants/actionTypes";

export const setCards = (cards) => {
    return {
        type: ActionTypes.SET_CARDS,
        payload: cards
    }
}