
import React from "react";
import CardListing from "./component/cardList";
import "./App.css";

function App() {
  return (
    <div className="App">
      <CardListing />
    </div>
  );
}

export default App;