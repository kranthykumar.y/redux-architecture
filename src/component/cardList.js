import React, { useEffect } from 'react';
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { setCards } from '../redux/actions/actions';

const CardList = () => {
    const cards = useSelector((state) => state.allCards.cards);
    const dispatch = useDispatch();
    const fetchCards = async () => {
    const response = await axios.get("https://jsonplaceholder.typicode.com/posts").catch((err) => { console.log(err); });
        dispatch(setCards(response.data));
    }
    useEffect(() => {
        fetchCards();
    },[]);
    const renderList = cards.map((card) => {
        const {id, title, body} = card;
        return (
            <div className="container" key={id}>
              <div className="header">{title}</div>
              <div className="body">{body}</div>
            </div>
        );
    })
    return (
        <div>
            <h1>{renderList}</h1>
        </div>
    );
};

export default CardList;
